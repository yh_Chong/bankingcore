#!/bin/sh

LOG="/tmp/1_gitpushbanking.txt"
REPO_DIR="/SEGITWKDIR/GitBuild1/BANKING/CORE/"
cd $REPO_DIR > $LOG 2>&1 
if [[ -n $(git status -s) ]]; then
	git add --all . >> $LOG 2>&1
	git commit -m "Commit from IFS_ALDMO1" >> $LOG 2>&1
	git push -u origin master >> $LOG 2>&1
fi
