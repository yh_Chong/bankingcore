     h dftactgrp(*no)
     Dresult           S              7P 3
      * //****************************************************
      * // Sample program to demonstrate RPG Free conversion**
      * //
     C     'Start'       dsply
     c                   eval      result = division(2:1)
     C     '1 - RESULT'  DSPLY
     C     RESULT        DSPLY
     C                   eval      result = division(0:0)
     C     '2 - RESULT'  DSPLY
     C     RESULT        DSPLY
     C                   MOVE      '1'           *INLR
      * //****************************************************
     Pdivision         B
      *
     Ddivision         PI             7P 3
     D  DIVIDEND                      5P 0 const
     D  DIVISOR                       5P 0 const
     D RESULT          S              7P 3
     D ERRORHAPPEND    S               N
      *
     c                   monitor
     C                   EVAL      RESULT = DIVIDEND / DIVISOR
      *
     c                   on-excp   'MCH1211'
     c     'on-excp'     dsply
     c                   on-error  *program
     c     'on-error'    dsply
     c                   endmon
      *
     C                   RETURN    RESULT
     Pdivision         E
      * //****************************************************
