     H Optimize(*Basic) Option(*SrcStmt : *ShowCpy) DftActGrp(*No)
     H Alwnull(*InputOnly)

      *----------------Variables
     D i               s              4s 0 Inz
     D from            s              4s 0 Inz(0001)
     D to              s              4s 0 Inz
     D plus            s              4s 0 Inz
     D  ID             s              9a   Inz
     D $HowManyIDs     s              4s 0 Inz

      *----------------Definitions for Prototype & Signature
     D GetGuest        PR                  Extpgm('GETGST000R')
     D  IDs                         200a
     D $Status                        1a
     D $StatusReason                  1a
     D $Msg                          80a

     D  GetGuest       PI
     D  IDs                         200a
     D $Status                        1a
     D $StatusReason                  1a
     D $Msg                          80a

      *----------------Sub-Procedures
     D DoDefaults      PR

      *----------------SQL Statement
       dcl-s currentTime time;
       EXEC SQL
                SET :CURRENTTIME = CURRENT_TIME;

       //----------------------------------------------------------------------
       //               M A I N    P R O C E D U R E
       //----------------------------------------------------------------------
           Select;
             When  IDs <> *Blank;
                   Monitor;
                      DoDefaults();
                   On-error;
                      *inLR = *On;
                      Return;
                   Endmon;
             Other;
                $Msg = 'Error: Need a Person ID';
                *inLR = *On;
                Return;
           Endsl;
              *inLR = *On;
              Return;

       //----------------------------------------------------------------------
       //         R O U T I N E S  /  S U B P R O C E D U R E S
       //----------------------------------------------------------------------
     P DoDefaults      B
     D DoDefaults      PI
         Clear $HowManyIDs;
         Reset i   ;
         Reset from;
         Reset to  ;
         Reset plus;
         For  i = 01 to 20;
              to = %scan('$':IDs:from);                          // over parm va
              If  to > 0;
                  plus = to - from;
                  EvalR ID     = %trim (%subst (IDs:from:plus)); // over work va
                        ID     = %xlate(' ':'0':ID);
                  from = to + 1;
                  $HowManyIDs += 1;
                  Else;
                    Leave;
              Endif;
         Endfor;
     P DoDefaults      E

      **************************************************************************
